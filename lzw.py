import cv2
import numpy as np

img = cv2.imread('image_gray.bmp', cv2.IMREAD_GRAYSCALE)

def initiate_dic():
    dic = {}
    for i in range(256):
        dic[chr(i)] = i
    return dic

def encode_lzw(letters):
    dic = initiate_dic()
    index = 256
    max_index = 256 * 256

    last_seq = letters[0]
    new_key = ''

    # send seq
    seq = ''
    out_stream = [dic[last_seq]]

    for letter in letters[1:]:
        # find valid sequence, not send
        if seq + letter in dic:
            seq += letter
            continue

        # seq je validni, mohu ji poslat
        out_stream.append(dic[seq])

        # hledam nevalidni new_seq pomoci last_seq a seq
        new_seq = seq
        for i in range(len(last_seq) + 1):
            new_seq = last_seq[-i] + new_seq
            if new_seq not in dic:
                break

        # print(f'{index}-{new_seq}', 'HEUREKA')
        if index < max_index:
            dic[new_seq] = index
            index += 1

        # last_seq je seq
        last_seq = seq
        seq = letter

    out_stream.append(dic[seq])
    return out_stream


def initiate_list():
    list = []
    for i in range(256):
        list.append(chr(i))
    return list


def initiate_set():
    list = []
    for i in range(256):
        list.append(chr(i))
    return set(list)


def decode_lzw(encoded):
    keys = initiate_set()
    dictionary = initiate_list()
    index = 256
    max_index = 256 * 256
    last_seq = dictionary[encoded[0]]
    decoded = dictionary[encoded[0]]

    for number in encoded[1:]:
        seq = dictionary[number]

        decoded += seq

        new_seq = seq
        for i in range(len(last_seq) + 1):
            new_seq = last_seq[-i] + new_seq
            if new_seq not in keys:
                break

        # print(f'{index}-{new_seq} NEW ENTRY')
        if index < max_index:
            keys.add(new_seq)
            dictionary.append(new_seq)
            index += 1

        # last_seq je seq
        last_seq = seq

    return decoded



# image = img.flatten()
#
# image_str = [chr(i) for i in image]
# print('image', image, len(image))
# #image_str = 'aaaaaaaaaaaaaaa'
# encoded = np.array(encode_lzw(image_str))
# print('encoded', encoded)
# print(f'encoded length: {len(encoded)}')
# decoded_str = decode_lzw(encoded)
# decoded = np.array([ord(d) for d in decoded_str])
# print('decoded', decoded, len(decoded))

