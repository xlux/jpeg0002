import numpy as np
import math


def compose_img(a, h, v, d):
    upper = np.concatenate((a, h), axis=1)
    lower = np.concatenate((v, d), axis=1)
    result = np.concatenate((upper, lower), axis=0)
    return result


def decompose_img(img):
    upper, lower = np.vsplit(img, 2)
    a, h = np.hsplit(upper, 2)
    v, d = np.hsplit(lower, 2)
    return a, h, v, d


def subtract_modulo(a, lossy):
    ak = (a < 0) * ((a + lossy - 1) % lossy - lossy + 1) + (a > 0) * a % lossy
    assert ak.all() % lossy == 0
    assert np.abs(ak).all() <= np.abs(a).all()
    return a - ak


def subtract_mod(a, lossy):
    if a >= 0:
        return a - a % lossy
    b = a - 1
    return (b + lossy) - (b % lossy)


def dwt_dec2(img, levels=1, lossy=1):
    if levels == 0:
        return img

    m, n = img.shape
    m_half = int(m // 2)
    n_half = int(n // 2)
    assert m % 2 ** levels == 0, f'incorrect dimension {m}'
    assert n % 2 ** levels == 0, f'incorrect dimension {n}'

    h = np.zeros((m_half, n_half), np.int32)
    v = np.zeros((m_half, n_half), np.int32)
    d = np.zeros((m_half, n_half), np.int32)
    a = np.zeros((m_half, n_half), np.int32)

    img = img.astype(np.int32)

    for i in range(m_half):
        for j in range(n_half):
            h[i, j] = img[2 * i, 2 * j + 1] - img[2 * i, 2 * j]
            h[i, j] = subtract_mod(h[i, j], lossy)
            v[i, j] = img[2 * i + 1, 2 * j] - img[2 * i, 2 * j]
            v[i, j] = subtract_mod(v[i, j], lossy)
            d[i, j] = img[2 * i + 1, 2 * j + 1] - img[2 * i, 2 * j]
            d[i, j] = subtract_mod(d[i, j], lossy)
            a[i, j] = img[2 * i, 2 * j] + (h[i, j] + v[i, j] + d[i, j]) // 4
            neighborhood = [img[2 * i, 2 * j],
                            img[2 * i + 1, 2 * j],
                            img[2 * i, 2 * j + 1],
                            img[2 * i + 1, 2 * j + 1]]
            # assert if approximate value don not increase the range
            assert a[i, j] <= np.max(neighborhood),\
                (a[i, j], neighborhood, h[i, j], v[i, j], d[i, j])
            assert a[i, j] >= np.min(neighborhood),\
                (a[i, j], neighborhood, h[i, j], v[i, j], d[i, j])
    a = dwt_dec2(a, levels=levels-1, lossy=1)

    h = h // lossy + 128
    v = v // lossy + 128
    d = d // lossy + 128

    assert h.all() >= 0
    assert v.all() >= 0
    assert d.all() >= 0

    result = compose_img(a, h, v, d)
    return result


def dwt_rec2(img, levels=1, lossy=1):
    if levels == 0:
        return img

    m, n = img.shape
    m_half = int(m // 2)
    n_half = int(n // 2)

    img.astype(np.int32)

    a, h, v, d = decompose_img(img)

    a = dwt_rec2(a, levels=levels-1, lossy=1)

    h = h - 128
    v = v - 128
    d = d - 128

    assert h.all() >= 0
    assert v.all() >= 0
    assert d.all() >= 0

    h = h * lossy
    v = v * lossy
    d = d * lossy

    result = np.zeros((m, n))
    for i in range(m_half):
        for j in range(n_half):
            result[2 * i, 2 * j] = a[i, j] - (h[i, j] + v[i, j] + d[i, j]) // 4
            result[2 * i, 2 * j + 1] = h[i, j] + result[2 * i, 2 * j]
            result[2 * i + 1, 2 * j] = v[i, j] + result[2 * i, 2 * j]
            result[2 * i + 1, 2 * j + 1] = d[i, j] + result[2 * i, 2 * j]

    assert result.all() >= 0
    return result
