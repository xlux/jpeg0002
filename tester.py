import numpy as np
from random import randint
from j2m import store_j2m, load_j2m

if __name__ == '__main__':

    numbers = np.load('enc0.npy')
    store_j2m('encoded.j2m', numbers)
    result = load_j2m('encoded.j2m')
    print(numbers)
    print(result)
    assert numbers.all() == result.all()





