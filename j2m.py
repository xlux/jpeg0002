import click
import numpy as np
import cv2
import os
from lzw import encode_lzw, decode_lzw
from dwt import dwt_rec2, dwt_dec2
from condense import condense, expand


def store_j2m(filename, numbers, bitdepth=9):

    depth_limit = 256
    index = 0
    result = ''

    # write numbers as bit-stream
    for i, symbol in enumerate(numbers):
        result += ("{0:b}".format(symbol)).zfill(bitdepth)

        # increase bitdepth
        index += 1
        if index == depth_limit:
            index = 0
            depth_limit *= 2
            bitdepth += 1

    # encode bit-stream to 8bit numbers
    length = len(result)
    n_chars = length // 8

    out_stream = []
    for n in range(n_chars):
        out_stream.append(int(result[n * 8:(n + 1) * 8], 2))

    if length % 8 != 0:
        last_number = result[n_chars * 8:(n_chars + 1) * 8]
        while len(last_number) < 8:
            last_number = last_number + '0'
        out_stream.append(int(last_number, 2))

    # encoded stream
    with open(filename, 'wb') as f:
        f.write(bytes(out_stream))


def load_j2m(filename, bitdepth=9):
    # decode result from chars
    with open(filename, 'rb') as f:
        in_stream = f.read()

    # store 8bit numbers to a bit-stream
    bit_stream = ''
    for number in in_stream:
        bit_stream += "{0:b}".format(number) .zfill(8)

    recieved = []
    depth_limit = 256
    index = 0
    dec_ptr = 0

    # read bit-stream according the current bitdepth
    while len(bit_stream) - dec_ptr >= bitdepth:
        word = bit_stream[dec_ptr:dec_ptr+bitdepth]
        recieved.append(int(word, 2))
        dec_ptr += bitdepth
        index += 1

        # increase bit-depth
        if index == depth_limit:
            index = 0
            depth_limit *= 2
            bitdepth += 1

    return np.array(recieved)


def j2m_encoder(img, levels=0, lossy=1):
    img, values = condense(img)
    # print(np.min(img), np.max(img))
    img = dwt_dec2(img.astype(np.int), levels, lossy=lossy)
    img = img.flatten()
    # print(np.min(img), np.max(img))
    values = [str(int(v)) for v in values]
    values_str = [chr(int(''.join(values[i*8:(i+1)*8]), 2)) for i in range(32)]
    img_str = values_str + [chr(i) for i in img]
    encoded = encode_lzw(img_str)
    return encoded


def j2m_decoder(result, levels=0, lossy=1):
    decoded_str = decode_lzw(result)
    decoded = [ord(d) for d in decoded_str]
    values = decoded[:32]
    values = [list(("{0:b}".format(v)).zfill(8)) for v in values]
    values = np.array(values).flatten() == '1'
    pixels = [int(d) for d in decoded[32:]]
    img = np.array(pixels).reshape((400, 600))
    img = dwt_rec2(img.astype(np.int), levels, lossy=lossy)
    img = expand(img, values)
    return img.astype(np.uint8)


def test(img, steps=0):
    img = dwt_dec2(img, steps)
    img = img.flatten()
    img_str = [chr(i) for i in img]
    encoded = encode_lzw(img_str)
    #
    decoded_str = decode_lzw(encoded)
    decoded = [ord(d) for d in decoded_str]
    img2 = np.array(decoded).reshape((400, 600))
    img2 = dwt_rec2(img2, steps)

    assert img.all() == img2.all()
    return img2


@click.command()
@click.option('--path', prompt='file path',
              help='file path')
@click.option('--encode/--decode', default=True,
              help='encode or decode procedure')
@click.option('--levels', default=0,
              help='number of dwt levels')
@click.option('--compress', default=1,
              help='level of compress')
def j2m(path='', encode=True, levels=0, compress=1):
    if not os.path.isfile(path):
        print('ERROR: not valid file path')
        return

    # img = cv2.imread(path, cv2.IMREAD_GRAYSCALE)
    # test(img)

    if encode:
        img = cv2.imread(path, cv2.IMREAD_GRAYSCALE)
        result = j2m_encoder(img, levels, lossy=compress)
        store_j2m(path + '.j2m', result)
        print('encoding to J2M was successful')
        rate = 100 * os.path.getsize(path + '.j2m') / os.path.getsize(path)
        print('Compression rate: {:.2f} %'.format(rate))

    else:
        result = load_j2m(path)
        img = j2m_decoder(result, levels, lossy=compress)
        cv2.imwrite(path + '.bmp', img)


if __name__ == '__main__':
    j2m()
