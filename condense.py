import numpy as np


def condense(img):
    result = np.zeros(img.shape, dtype=np.uint8)
    values = np.zeros((256, 1), dtype=bool)
    values = values.flatten()
    new_index = 0

    for i in range(256):
        mask = (img == i).astype(np.uint8)
        if np.sum(mask) != 0:
            result += new_index * mask
            new_index += 1
            values[i] = True

    # TODO store values as bytes

    return result, values


def expand(img, values):
    # TODO read values as bytes

    # print('expand', np.min(img), np.max(img))

    result = np.zeros(img.shape)
    old_index = 0

    for i in range(256):
        while old_index < 256 and not values[old_index]:
            old_index += 1
        mask = img == i
        if np.sum(mask) != 0 and old_index < 256:
            result += old_index * mask
            values[old_index] = False

    return result
